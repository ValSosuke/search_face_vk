from django.contrib import admin
from django.urls import path, include
from . import views
#from django.views.decorators.csrf import csrf_exempt
urlpatterns = [
    path('load_file/', views.show_load_file_html_form)
]