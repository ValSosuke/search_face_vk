from django.shortcuts import render
from .forms import upload_image_forms
import face_recognition
from . import models
import pickle
from django.http import HttpResponse
import numpy

def list_item_list_face(llist):
    res = []
    counter = 0
    for i in llist:
        if(i==True):
            res.append(counter)
        counter+=1
    return res

def handle_uploaded_file(f): #Обработчик файла
    ffile = open('appSearcher/load_images/'+f.name, 'wb')
    for chunk in f.chunks():
        ffile.write(chunk)
    ffile.close()
    #получение весов
    unknown_picture = face_recognition.load_image_file('appSearcher/load_images/'+f.name)
    unknown_face_encoding = face_recognition.face_encodings(unknown_picture)[0]
    #поиск
    query_all = models.Musician.objects.all()
    querydict_images = [pickle.loads(i.serialized_object_numpy_ndarray) for i in query_all]
    results = face_recognition.compare_faces(querydict_images, unknown_face_encoding)
    list_of_href_images = [] # список ссылок на фотки, передаваемые шаблону

    for i in list_item_list_face(results):
        list_of_href_images.append(query_all[i].reference_on_picture)
    return list_of_href_images
    #for query_image in querydict_images:

def show_load_file_html_form(request):
    context = {}
    if request.method == 'GET':
        form = upload_image_forms.UploadFileForm()
        context['file'] = form
        return render(request, 'appSearcher/form_for_upload_image.html', context)
    elif request.method == 'POST':
        form = upload_image_forms.UploadFileForm(request.POST, request.FILES)
        #context['file'] = form
        if form.is_valid():
            context['href_images'] = handle_uploaded_file(request.FILES['file'])
            return render(request, 'appSearcher/comparison_result.html', context)
        else:
            return HttpResponse(request, 'Иди в пизду')
# Create your views here.
