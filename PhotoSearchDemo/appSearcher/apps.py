from django.apps import AppConfig


class AppsearcherConfig(AppConfig):
    name = 'appSearcher'
