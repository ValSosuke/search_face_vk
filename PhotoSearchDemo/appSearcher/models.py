from django.db import models

# Create your models here.
class Musician(models.Model):
    WEB_SITES = (('VK','Vkontakte'),('OK','Odnoklassniki'),('I','Instagram'), ('F','Facebook'))
    website = models.CharField(max_length=2, choices = WEB_SITES)
    id_people = models.CharField(max_length = 100)
    reference_on_picture = models.CharField(max_length = 200)
    serialized_object_numpy_ndarray = models.BinaryField(max_length = 2000)
